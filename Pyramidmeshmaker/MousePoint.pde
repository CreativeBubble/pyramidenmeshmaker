class MousePoint{
    int x,y,z;
    color pointcolor = color(000,255,000);

    MousePoint(int _x, int _y, int _z){
      x = _x;
      y = _y;
      z = _z;
    }

    void update(){
      drawSelf();
    }

    void drawSelf(){
      pushMatrix();
      strokeWeight(3);
      stroke(pointcolor);
      point(x,y,z);
      popMatrix();
    }
}
