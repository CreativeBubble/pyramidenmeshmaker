class Pyramid_S3 {


  Point a, b, c;
  Point top;
  color edge_color =  color(255, 255, 255);
  int texture_bottom_y = 310+250;
  int texture_top_y = 100+250;
  int texture_bottom_y_actual = 310;
  int texture_top_y_actual = 100;
  PImage textur1 = loadImage("lines_high_kontrast.jpg");


  Pyramid_S3(Point _a, Point _b, Point _c, Point _top) {
    a = _a;
    b = _b;
    c = _c;
    top = _top;
  }
/*
  Point get_points(int i) {
    if (i == 0) {
      return a;
    } else if (i == 1) {
      return b;
    } else if (i == 2) {
      return c;
    } else (i == 3) {
      return top;
    }
  }
*/
  Point get_a(){return a;}
  Point get_b(){return b;}
  Point get_c(){return c;}


  void drawbottomlayer() {
    noStroke();
    fill(125);
    beginShape();
    texture(textur1);
    vertex(a.x, a.y, a.z);
    vertex(b.x, b.y, b.z);
    vertex(c.x, c.y, c.z);
    endShape();
  }

  void drawsidelayers() {
    pushMatrix();
    stroke(255);
    strokeWeight(0);
    fill(250);
    noStroke();
    beginShape();
  //  texture(textur1);
    vertex(a.x, a.y, a.z);
    vertex(b.x, b.y, b.z);
    vertex(top.x, top.y, top.z);
  //  vertex(a.x, a.y, a.z,50,texture_bottom_y_actual);
  //  vertex(b.x, b.y, b.z,250,texture_bottom_y_actual);
  //  vertex(top.x, top.y, top.z,50,texture_top_y_actual);
    endShape();
    fill(255);
    beginShape();
  //  texture(textur1);
    vertex(a.x, a.y, a.z);
    vertex(c.x, c.y, c.z);
    vertex(top.x, top.y, top.z);
  //  vertex(a.x, a.y, a.z,50,texture_bottom_y_actual);
  //  vertex(c.x, c.y, c.z,250,texture_bottom_y_actual);
  //  vertex(top.x, top.y, top.z,50,texture_top_y_actual);
    endShape();
    fill(255);
    beginShape();
  //  texture(textur1);
    vertex(a.x, a.y, a.z);
    vertex(c.x, c.y, c.z);
    vertex(top.x, top.y, top.z);
  //  vertex(b.x, b.y, b.z,50,texture_bottom_y_actual);
  //  vertex(c.x, c.y, c.z,250,texture_bottom_y_actual);
  //  vertex(top.x, top.y, top.z,50,texture_top_y_actual);
    endShape();
    popMatrix();
    if(frameCount%2 == 0){
      texture_top_y_actual = texture_top_y_actual-1;
      texture_bottom_y_actual = texture_bottom_y_actual-1;
    }
    if((texture_top_y_actual-texture_top_y)%250 == 0){
      texture_top_y_actual = texture_top_y;
      texture_bottom_y_actual = texture_bottom_y;
    }

  }

  void drawSelf() {
    fill(128);
    strokeWeight(2);
    stroke(180);
    beginShape();
    line(a.x, a.y, a.z, b.x, b.y, b.z);
    line(a.x, a.y, a.z, top.x, top.y, top.z);
    line(b.x, b.y, b.z, c.x, c.y, c.z);
    line(b.x, b.y, b.z, top.x, top.y, top.z);
    line(a.x, a.y, a.z, c.x, c.y, c.z);
    line(c.x, c.y, c.z, top.x, top.y, top.z);
    endShape();
  }

  boolean intersect_with_balls(){
    return false;

  }

  void draw_dashed_lines_mesh(){

  }
}
